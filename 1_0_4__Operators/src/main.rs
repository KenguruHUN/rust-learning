use std::mem;

const MEANING_OF_LIFE:u8 = 42;  // no fix address

static Z:i32 = 123;  // this is a static variable

static mut Y:i32 = 1234;  //this throws an error, because it is an unsafe operation

fn operators () {
    //arithmetic

    let mut a = 2 + 3 * 4;
    println!("{}", a);

    a = a + 1;  // rust does not support the -- and the ++ operator
    println!("{}", a);

    a += 1;
    println!("{}", a);

    println!("remainder of {} / {} = {}", a, 3, (a%3));

    let a_cubed = i32::pow(a, 3);
    println!("{} cubed  is {}", a, a_cubed);

    let b = 2.5;
    let b_cubed = f64::powi(b, 3);  // powi means the power is an integer
    println!("{} cubed  is {}", b, b_cubed);

    let b_to_pi = f64::powf(b, std::f64::consts::PI);
    println!("{}^pi = {}", b, b_to_pi);

    // bitwise ops

    let c = 1 | 2;  // | is the OR operation
                         // 01 or 10 == 11 == 3_10

    println!("1|2 = {}", c);

    let two_to_10 = 1 << 10;
    println!("2^10 = {}", two_to_10);

    // logical ops

    let pi_less_4 = std::f64::consts::PI < 4.0; // True
    println!("Is pi is less than 4 ? {}", pi_less_4);

    let x = 5;
    let x_is_5 = x == 5;

    println!("{}", x_is_5);
}


fn scope_and_shadowing () {
    let a = 123;


    {
        let b = 456;
        println!("inside, b = {}", b);

        let a = 777;
        println!("inside, a = {}", a);
    }

    println!("outside, a = {}", a);

}

fn main () {
    println!("Normal constant, {}", MEANING_OF_LIFE);

    println!("Normal static, {}", Z);

    // if you want print out the Y static variable first of all declare an unsafe block.
    // This essentially your problem is that you're working with carefully with this variable, and you're not screwing hings up in this case
    unsafe {
        println!("Unsafe static, {}", Y);
    }

    println!("Operators function running");
    operators();

    println!("\nScope and shadowing function running");
    scope_and_shadowing();
}