use std::mem;

fn main() {

    let a:u8 = 123;  // a named variable, its unsigned (0 +) 8 bit integer
    println!("a = {}", a);

    //a = 456;  // drop error because by default the a value and all value is unmutable

    let mut b:i8 = 0;  // this variable is mutable
    println!("b = {}", b);
    b = 42;
    println!("b = {}", b);

    let mut c = 123456789;  // this is a 32 bits signed i32
    println!("c = {}, size = {} bytes", c, mem::size_of_val(&c));

    c = -1;
    println!("c = {}, after modification", c);

    // we have i8 u8 i16 u16 i32 u32 i64 u64

    let z:isize = 123;
    let size_of_z = mem::size_of_val(&z);
    println!("z = {}, takes up {} bytes, {}-bit os", z, size_of_z, size_of_z * 8);


    let d:char = 'x';  // a single char
    let d = 's'; // the compiler notice this is a string"
    println!("d = {}, takes up {} bytes", d, mem::size_of_val(&d));


    let e = 2.5; // double-precision, eight bytes or 64bits, f64
    println!("e = {}, takes up {} bytes", e, mem::size_of_val(&e));

    // boolean
    let g = false;
    println!("g = {}, takes up {} bytes", g, mem::size_of_val(&g));

    let f = 4 > 0;  // true


}
