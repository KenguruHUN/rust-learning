// option<T>

struct Point<T> {
    x: T,
    y: T
}

struct Line<T>{
    start: Point<T>,
    end:   Point<T>
}

fn generics(){
    let a:Point<f64> = Point {x: 0.0, y: 4f64};  // a:Point<f64>
    let b = Point {x: 1.2, y: 3.4};

    // because of different type of 'a' and 'b' this throw error
    // for fixing this need to be the points same type
    let myline = Line {start: a, end: b};
}

fn main() {
    generics();
}
