


fn vectors() {
    let mut a = Vec::new();

    a.push(1);
    a.push(2);
    a.push(3);

    a.push(44);

    println!("a {:?}", a);

    //accessing vectors first element
    println!("first element of the vector is {}", a[0]);

    // index of array and vector is usize or isize

    //let idx:i32 = 0;
    // println!("a[0] = {}", a[idx]); throws error

    let idx:usize = 0;

    println!("a[0] = {}", a[idx]);

    // use get, it return an option type
    match a.get(6) {
        Some(x) => println!("a[6] = {}", x),
        None => println!("error, no such element")
    }

    //iterating
    for x in &a {
        println!("{}", x);
    }

    //adding element to vector
    a.push(64);

    println!("{:?}", a);

    // remove the last element from vector
    let last_element = a.pop();

    // pop is an option too
    println!("last element is {:?}, a = {:?}", last_element, a) ;

    while let Some(x) = a.pop() {
        println!("{}", x);
    }
}

fn main() {
    vectors();
    // arrays();
}
